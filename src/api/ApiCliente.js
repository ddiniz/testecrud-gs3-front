import Api from "./Api.js";

const ApiCliente = {
  path: "cliente",

  async save(cliente) {
    return await Api.post(`${this.path}/`, cliente);
  },
  async update(cliente) {
    return await Api.put(`${this.path}/`, cliente);
  },
  async list() {
    return await Api.get(`${this.path}/list`);
  },
  async find(id) {
    return await Api.get(`${this.path}/${id}`);
  },
  async delete(id) {
    return await Api.delete(`${this.path}/${id}`);
  },
};

export default ApiCliente;
