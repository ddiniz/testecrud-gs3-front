import Api from "./Api.js";
import AuthStorage from "./AuthStorage.js";
const ApiAuth = {
  path: "auth",
  async hello() {
    return await Api.get(`${this.path}/hello`);
  },
  async loginRequest(user) {
    return await Api.post(`${this.path}/login`, user);
  },
  async login(user) {
    let data = null;
    let error = null;

    await this.loginRequest(user)
      .then((res) => {
        if (res.data) data = res.data;
      })
      .catch((e) => (error = e));
    if (data) {
      AuthStorage.salvar(data);
      return data;
    }

    if (error) throw error;
  },
  logoff() {
    sessionStorage.clear();
    localStorage.clear();
  },
};

export default ApiAuth;
