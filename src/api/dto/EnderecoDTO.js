export default class EnderecoDTO {
  id = 0;
  cep = "";
  logradouro = "";
  bairro = "";
  cidade = "";
  complemento = "";
  numero = "";
  uf = "";
  constructor(obj) {
    if (obj) {
      this.id = obj.id;
      this.cep = obj.cep;
      this.logradouro = obj.logradouro;
      this.bairro = obj.bairro;
      this.cidade = obj.cidade;
      this.complemento = obj.complemento;
      this.numero = obj.numero;
      this.uf = obj.uf;
    }
  }
}
