import EnderecoDTO from "./EnderecoDTO.js";
export default class ClienteDTO {
  id = 0;
  nome = "";
  cpf = "";
  emails = [];
  telefones = [];
  endereco = new EnderecoDTO();
}
