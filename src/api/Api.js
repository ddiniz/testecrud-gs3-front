import axios from "axios";
import AuthStorage from "./AuthStorage.js";
//import UrlHelper from "@/utils/UrlHelper.js";
//import Loader from "@/utils/Loader.js";
//import Alerta from "@/utils/Alerta.js";

const Api = axios.create({
  baseURL: "http://localhost:8080",
  withCredentials: true,
});
Api.all = axios.all;
Api.spread = axios.spread;
Api.interceptors.request.use((config) => {
  const jwt = AuthStorage.token();

  if (jwt) config.headers.Authorization = `Bearer ${jwt}`;

  return config;
});
Api.interceptors.response.use(
  (config) => {
    return config;
  },
  (error) => {
    try {
      if (error) {
        if (
          error.response != null &&
          error.response.data != null &&
          error.config != null &&
          error.config.method != null &&
          error.config.url != null
        ) {
          console.log(
            error.config.method,
            error.config.url,
            error.response.status,
            error.response.data
          );
        } else console.log(error);
      } else console.log(error);
    } catch (e) {
      console.log(e);
    }
    return Promise.reject(error);
  }
);

Api.logout = () => {
  sessionStorage.clear();
  localStorage.clear();
};

Api.erroPadrao = (error) => {
  let erroMsg = "";
  let stackTrace = "";
  if (error && error.response != null && error.response.data != null) {
    if (
      error.response.data.status != null &&
      error.response.data.status === "SEE_OTHER"
    ) {
      erroMsg = error.response.data.msg;
      stackTrace = error.response.data.stackTrace;
      //Alerta.erroStackTrace(erroMsg, stackTrace);
      return;
    } else {
      erroMsg = `${error.response.status} - ${error.response.data} `;
    }
  } else {
    erroMsg = `Erro desconhecido. Por favor entre em contato com o suporte.`;
  }
  //Alerta.erro(erroMsg);
};

// Api.BNLRCGet = async (path, params, showLoader = true, showError = true) => {
//   if (showLoader) Loader.on();
//   return await Api.get(path, params)
//     .then((response) => {
//       return response.data;
//     })
//     .catch((error) => {
//       if (showError) Api.erroPadrao(error);
//       throw error;
//     })
//     .finally(() => {
//       if (showLoader) Loader.off();
//     });
// };
// Api.BNLRCPost = async (
//   path,
//   requestObject,
//   showLoader = true,
//   showError = true
// ) => {
//   if (showLoader) Loader.on();
//   return await Api.post(path, requestObject)
//     .then((response) => {
//       return response.data;
//     })
//     .catch((error) => {
//       if (showError) Api.erroPadrao(error);
//       throw error;
//     })
//     .finally(() => {
//       if (showLoader) Loader.off();
//     });
// };
// Api.BNLRCPut = async (
//   path,
//   requestObject,
//   showLoader = true,
//   showError = true
// ) => {
//   if (showLoader) Loader.on();
//   return await Api.put(path, requestObject)
//     .then((response) => {
//       return response.data;
//     })
//     .catch((error) => {
//       if (showError) Api.erroPadrao(error);
//       throw error;
//     })
//     .finally(() => {
//       if (showLoader) Loader.off();
//     });
// };
// Api.BNLRCDelete = async (path, params, showLoader = true, showError = true) => {
//   if (showLoader) Loader.on();
//   return await Api.delete(path, params)
//     .then((response) => {
//       return response.data;
//     })
//     .catch((error) => {
//       if (showError) Api.erroPadrao(error);
//       throw error;
//     })
//     .finally(() => {
//       if (showLoader) Loader.off();
//     });
// };
export default Api;
