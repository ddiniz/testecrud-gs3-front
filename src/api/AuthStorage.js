const AuthStorage = {
  parseJwt(token) {
    var base64Url = token.split(".")[1];
    var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
    var jsonPayload = decodeURIComponent(
      atob(base64)
        .split("")
        .map(function (c) {
          return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
        })
        .join("")
    );

    return JSON.parse(jsonPayload);
  },

  salvar(token) {
    const auth = {
      token,
      parsed: this.parseJwt(token),
    };

    localStorage.setItem("auth", JSON.stringify(auth));
  },
  token() {
    try {
      const auth = JSON.parse(localStorage.getItem("auth"));
      return auth && auth.token;
    } catch (e) {
      console.log(e);
    }
    return null;
  },
  parsed() {
    try {
      const auth = JSON.parse(localStorage.getItem("auth"));
      return auth && auth.parsed;
    } catch (e) {
      console.log(e);
    }
    return null;
  },
  isLogado() {
    const parsedToken = AuthStorage.token() && AuthStorage.parsed();
    if (!parsedToken) return false;
    const currentDate = new Date();
    const expirationDate = new Date(parsedToken.exp * 1000);

    return currentDate.getTime() < expirationDate.getTime();
  },
  isAdmin() {
    if (this.isLogado()) {
      const jwtParsed = AuthStorage.parsed();
      return jwtParsed.permissao && jwtParsed.permissao.includes("ADMIN");
    }
    return false;
  },
};
export default AuthStorage;
