import Api from "./Api.js";

const ApiCEP = {
  path: "cep",

  async getCEP(cep) {
    return await Api.get(`${this.path}/${cep}`);
  },
};

export default ApiCEP;
