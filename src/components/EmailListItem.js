import "../App.css";
import TextField from "@material-ui/core/TextField";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import CancelIcon from "@material-ui/icons/Cancel";
import CheckIcon from "@material-ui/icons/Check";
import React from "react";
import { checkEmail } from "../utils/FieldValidator.js";
import AuthStorage from "../api/AuthStorage.js";
export default class EmailListItem extends React.Component {
  state = { email: this.props.email.email, editando: false, emailEdit: null };
  edit = () => {
    this.props.edit(this.state.email, this.state.emailEdit);
    this.setState(() => ({ editando: false }));
  };
  cancel = () => {
    this.setState(() => ({ editando: false }));
  };
  delete = () => {
    this.props.delete(this.state.email);
  };
  changeToEdit = () => {
    this.setState(() => ({ emailEdit: this.state.email, editando: true }));
  };
  changeEmail = (e) => {
    this.setState(() => ({ emailEdit: e.target.value, editando: true }));
  };
  render() {
    return (
      <div>
        {this.state.editando ? (
          <div>
            <TextField
              id="email"
              label="Email"
              variant="outlined"
              type="email"
              placeholder="email@example.com"
              value={this.state.emailEdit}
              onChange={this.changeEmail}
              fullWidth
              error={!checkEmail(this.state.emailEdit)}
            />
            <IconButton
              aria-label="check"
              onClick={this.edit}
              disabled={!checkEmail(this.state.emailEdit)}>
              <CheckIcon />
            </IconButton>
            <IconButton aria-label="cancel" onClick={this.cancel}>
              <CancelIcon />
            </IconButton>
          </div>
        ) : (
          <div>
            {this.state.email}
            <IconButton
              aria-label="edit"
              onClick={this.changeToEdit}
              disabled={!AuthStorage.isAdmin()}>
              <EditIcon />
            </IconButton>
            <IconButton
              aria-label="delete"
              onClick={this.delete}
              disabled={!AuthStorage.isAdmin()}>
              <DeleteIcon />
            </IconButton>
          </div>
        )}
      </div>
    );
  }
}
