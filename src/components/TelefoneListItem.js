import "../App.css";
import TextField from "@material-ui/core/TextField";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import CancelIcon from "@material-ui/icons/Cancel";
import CheckIcon from "@material-ui/icons/Check";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import TelefoneDTO from "../api/dto/TelefoneDTO.js";
import React from "react";
import InputMask from "react-input-mask";
import AuthStorage from "../api/AuthStorage.js";

export default class TelefoneListItem extends React.Component {
  state = {
    telefone: this.props.telefone,
    editando: false,
    telefoneEdit: new TelefoneDTO(),
  };
  edit = () => {
    this.props.edit(this.state.telefone, this.state.telefoneEdit);
    this.setState(() => ({ editando: false }));
  };
  cancel = () => {
    this.setState(() => ({ editando: false }));
  };
  delete = () => {
    this.props.delete(this.state.telefone);
  };
  changeToEdit = () => {
    this.setState(() => ({
      telefoneEdit: this.state.telefone,
      editando: true,
    }));
  };
  changeTelefone = (e) => {
    this.setState((prevState) => ({
      telefoneEdit: {
        ...prevState.telefoneEdit,
        telefone: e.target.value,
      },
    }));
  };
  changeTipo = (e) => {
    this.setState((prevState) => ({
      telefoneEdit: {
        ...prevState.telefoneEdit,
        tipo: e.target.value,
      },
    }));
  };
  getTelefoneTipoString(tipo) {
    switch (tipo) {
      default:
      case 0:
        return "RESIDENCIAL";
      case 1:
        return "CELULAR";
      case 2:
        return "COMERCIAL";
    }
  }
  render() {
    return (
      <div>
        {this.state.editando ? (
          <div>
            <InputMask
              mask={
                this.state.telefoneEdit.tipo === 1
                  ? "(99) 99999-9999"
                  : "(99) 9999-9999"
              }
              value={this.state.telefoneEdit.telefone}
              onChange={this.changeTelefone}
              disabled={!AuthStorage.isAdmin()}>
              {() => (
                <TextField
                  id="telefone"
                  label="Telefone"
                  variant="outlined"
                  fullWidth
                />
              )}
            </InputMask>
            <InputLabel id="tipo">Tipo Telefone</InputLabel>
            <Select
              labelId="tipo"
              id="tipoTelefone"
              value={
                this.state.telefoneEdit.tipo ? this.state.telefoneEdit.tipo : 0
              }
              onChange={this.changeTipo}
              variant="outlined"
              fullWidth>
              <MenuItem value={0}>RESIDENCIAL</MenuItem>
              <MenuItem value={1}>CELULAR</MenuItem>
              <MenuItem value={2}>COMERCIAL</MenuItem>
            </Select>
            <IconButton
              disabled={this.state.telefoneEdit.telefone.includes("_")}
              aria-label="check"
              onClick={this.edit}>
              <CheckIcon />
            </IconButton>
            <IconButton aria-label="cancel" onClick={this.cancel}>
              <CancelIcon />
            </IconButton>
          </div>
        ) : (
          <div>
            {this.state.telefone.telefone} -
            {this.getTelefoneTipoString(this.state.telefone.tipo)}
            <IconButton
              aria-label="edit"
              onClick={this.changeToEdit}
              disabled={!AuthStorage.isAdmin()}>
              <EditIcon />
            </IconButton>
            <IconButton
              aria-label="delete"
              onClick={this.delete}
              disabled={!AuthStorage.isAdmin()}>
              <DeleteIcon />
            </IconButton>
          </div>
        )}
      </div>
    );
  }
}
