import "../App.css";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import React from "react";
import { checkEmail } from "../utils/FieldValidator.js";
import AuthStorage from "../api/AuthStorage.js";

export default class FormEmail extends React.Component {
  state = { email: "" };
  changeEmail = (e) => {
    this.setState({ email: e.target.value });
  };
  saveEmail = () => {
    this.props.saveEmail(this.state.email);
    this.setState({ email: "" });
  };
  render() {
    return (
      <div className="FormEmail">
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <TextField
              id="email"
              label="Email"
              type="email"
              placeholder="email@example.com"
              variant="outlined"
              value={this.state.email}
              onChange={this.changeEmail}
              disabled={!AuthStorage.isAdmin()}
              fullWidth
              error={!checkEmail(this.state.email)}
            />
          </Grid>
          <Grid item xs={12}>
            <Button
              disabled={
                this.state.email === null ||
                this.state.email.length === 0 ||
                !checkEmail(this.state.email) ||
                !AuthStorage.isAdmin()
              }
              variant="contained"
              color="primary"
              onClick={this.saveEmail}
              fullWidth>
              Salvar Email
            </Button>
          </Grid>
        </Grid>
      </div>
    );
  }
}
