import "../App.css";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import EmailListItem from "../components/EmailListItem.js";
import TelefoneListItem from "../components/TelefoneListItem.js";
import FormEmail from "../components/FormEmail.js";
import FormEndereco from "../components/FormEndereco.js";
import FormTelefone from "../components/FormTelefone.js";
import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";
import React from "react";

import ClienteDTO from "../api/dto/ClienteDTO.js";
import EmailDTO from "../api/dto/EmailDTO.js";
import EnderecoDTO from "../api/dto/EnderecoDTO.js";
import ApiCliente from "../api/ApiCliente.js";
import InputMask from "react-input-mask";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import AuthStorage from "../api/AuthStorage.js";

import {
  validateEndereco,
  validateCliente,
  checkCPF,
  checkStringMinMax,
} from "../utils/FieldValidator.js";

class FormCliente extends React.Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
  };
  state = {
    cliente: new ClienteDTO(),
    emails: [],
    telefones: [],
    endereco: new EnderecoDTO(),
    editando: false,
  };

  async componentDidMount() {
    if (this.props.id) {
      await ApiCliente.find(this.props.id)
        .then((response) => {
          this.setState({
            cliente: response.data,
            emails: response.data.emails,
            telefones: response.data.telefones,
            endereco: response.data.endereco,
            editando: true,
          });
        })
        .catch((e) => {
          alert(`Erro ao tentar achar cliente: ${e}`);
        });
    }
  }

  salvar = () => {
    let clienteRequest = this.state.cliente;
    clienteRequest.emails = this.state.emails;
    clienteRequest.telefones = this.state.telefones;
    ApiCliente.save(clienteRequest)
      .then((response) => {
        var { history } = this.props;
        history.replace("/cliente");
      })
      .catch((e) => {
        alert(`Erro ao salvar cliente : ${e}`);
      });
  };

  editar = () => {
    let clienteRequest = this.state.cliente;
    clienteRequest.emails = this.state.emails;
    clienteRequest.telefones = this.state.telefones;
    ApiCliente.update(clienteRequest)
      .then((response) => {
        var { history } = this.props;
        history.replace("/cliente");
      })
      .catch((e) => {
        alert(`Erro ao editar cliente: ${e}`);
      });
  };
  checkEmailExists(email) {
    for (let i = 0; i < this.state.emails.length; i++) {
      if (this.state.emails[i].email === email) {
        return true;
      }
    }
    return false;
  }
  saveEmail = (email) => {
    if (!this.checkEmailExists(email))
      this.setState({ emails: [...this.state.emails, new EmailDTO(email)] });
  };
  editEmail = (email, emailNovo) => {
    if (this.checkEmailExists(emailNovo)) return;
    let copia = [...this.state.emails];
    for (let i = 0; i < copia.length; i++) {
      if (copia[i].email === email) {
        copia[i].email = emailNovo;
        break;
      }
    }
    this.setState({ emails: copia });
  };
  deleteEmail = (email) => {
    let copia = [...this.state.emails];
    for (let i = 0; i < copia.length; i++) {
      if (copia[i].email === email) {
        copia.splice(i, 1);
        this.setState({ emails: copia });
        break;
      }
    }
  };

  saveTelefone = (telefoneObj) => {
    this.setState({ telefones: [...this.state.telefones, telefoneObj] });
  };
  editTelefone = (telefoneObj, telefoneObjNovo) => {
    let copia = [...this.state.telefones];
    for (let i = 0; i < copia.length; i++) {
      if (copia[i].telefone === telefoneObj.telefone) {
        copia[i] = telefoneObjNovo;
        break;
      }
    }
    this.setState({ telefones: copia });
  };
  deleteTelefone = (telefoneObj) => {
    let copia = [...this.state.telefones];
    for (let i = 0; i < copia.length; i++) {
      if (copia[i].telefone === telefoneObj.telefone) {
        copia.splice(i, 1);
        this.setState({ telefones: copia });
        break;
      }
    }
  };

  changeNome = (e) => {
    this.setState((prevState) => ({
      cliente: {
        ...prevState.cliente,
        nome: e.target.value,
      },
    }));
  };
  changeCPF = (e) => {
    this.setState((prevState) => ({
      cliente: {
        ...prevState.cliente,
        cpf: e.target.value,
      },
    }));
  };
  changeEndereco = (endereco) => {
    this.setState((prevState) => ({
      cliente: {
        ...prevState.cliente,
        endereco: endereco,
      },
    }));
  };

  dividerStyle = { marginTop: "1em", marginBottom: "1em" };
  h1Style = { fontWeight: "bold" };
  render() {
    return (
      <div className="FormCliente">
        <h1 style={this.h1Style}>
          {this.state.editando
            ? AuthStorage.isAdmin()
              ? "Editando cliente"
              : "Visualizando Cliente"
            : "Cadastro de novo cliente"}
        </h1>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <TextField
              id="nome"
              label="Nome"
              variant="outlined"
              fullWidth
              disabled={!AuthStorage.isAdmin()}
              helperText="Obrigatório, entre 3 e 100 caracteres"
              value={this.state.cliente.nome}
              onChange={this.changeNome}
              error={!checkStringMinMax(this.state.cliente.nome, 3, 100)}
            />
          </Grid>
          <Grid item xs={12}>
            <InputMask
              mask="999.999.999-99"
              value={this.state.cliente.cpf}
              onChange={this.changeCPF}
              disabled={!AuthStorage.isAdmin()}>
              {() => (
                <TextField
                  id="cpf"
                  label="CPF"
                  variant="outlined"
                  fullWidth
                  helperText="Obrigatório"
                  error={!checkCPF(this.state.cliente.cpf)}
                />
              )}
            </InputMask>
          </Grid>
        </Grid>
        <Divider style={this.dividerStyle} />
        <h1 style={this.h1Style}>Endereço</h1>
        <Divider style={this.dividerStyle} />
        <FormEndereco
          endereco={this.state.endereco}
          change={this.changeEndereco}
        />
        <Divider style={this.dividerStyle} />
        <h1 style={this.h1Style}>Emails</h1>
        <Divider style={this.dividerStyle} />
        <Grid container spacing={1}>
          <Grid item xs={6}>
            <FormEmail saveEmail={this.saveEmail} />
          </Grid>
          <Grid item xs={6}>
            {this.state.emails.length === 0
              ? "Insira pelo menos um email"
              : this.state.emails.map((obj) => (
                  <EmailListItem
                    key={obj.email + "_" + obj.id}
                    email={obj}
                    edit={this.editEmail}
                    delete={this.deleteEmail}
                  />
                ))}
          </Grid>
        </Grid>
        <Divider style={this.dividerStyle} />
        <h1 style={this.h1Style}>Telefones</h1>
        <Divider style={this.dividerStyle} />
        <Grid container spacing={1}>
          <Grid item xs={6}>
            <FormTelefone saveTelefone={this.saveTelefone} />
          </Grid>
          <Grid item xs={6}>
            {this.state.telefones.length === 0
              ? "Insira pelo menos um telefone"
              : this.state.telefones.map((obj) => (
                  <TelefoneListItem
                    key={obj.telefone + "_" + obj.tipo}
                    telefone={obj}
                    edit={this.editTelefone}
                    delete={this.deleteTelefone}
                  />
                ))}
          </Grid>
        </Grid>
        <Divider style={this.dividerStyle} />
        <Grid container spacing={1}>
          <Grid item xs={12}>
            {this.state.editando ? (
              <Button
                disabled={
                  !(
                    validateEndereco(this.state.cliente.endereco) &&
                    validateCliente(this.state.cliente) &&
                    this.state.emails.length > 0 &&
                    this.state.telefones.length > 0 &&
                    AuthStorage.isAdmin()
                  )
                }
                variant="contained"
                color="primary"
                onClick={this.editar}
                fullWidth>
                Editar Cliente
              </Button>
            ) : (
              <Button
                disabled={
                  !(
                    validateEndereco(this.state.cliente.endereco) &&
                    validateCliente(this.state.cliente) &&
                    this.state.emails.length > 0 &&
                    this.state.telefones.length > 0 &&
                    AuthStorage.isAdmin()
                  )
                }
                variant="contained"
                color="primary"
                onClick={this.salvar}
                fullWidth>
                Salvar Cliente
              </Button>
            )}
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withRouter(FormCliente);
