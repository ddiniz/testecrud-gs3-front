import "../App.css";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import React from "react";
import InputMask from "react-input-mask";
import AuthStorage from "../api/AuthStorage.js";

export default class FormTelefone extends React.Component {
  state = { telefone: "", tipo: 0, naoPodeSalvar: true };
  changeTelefone = (e) => {
    this.setState({ telefone: e.target.value });
  };
  changeTipo = (e) => {
    this.setState({ tipo: e.target.value });
  };
  saveTelefone = () => {
    this.props.saveTelefone({
      telefone: this.state.telefone,
      tipo: this.state.tipo,
    });
    this.setState({ telefone: "", tipo: 0 });
  };
  render() {
    return (
      <div className="FormTelefone">
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <InputMask
              mask={
                this.state.tipo === 1 ? "(99) 99999-9999" : "(99) 9999-9999"
              }
              value={this.state.telefone}
              onChange={this.changeTelefone}
              disabled={!AuthStorage.isAdmin()}>
              {() => (
                <TextField
                  id="telefone"
                  label="Telefone"
                  variant="outlined"
                  fullWidth
                />
              )}
            </InputMask>
          </Grid>
          <Grid item xs={12}>
            <InputLabel id="tipo">Tipo Telefone</InputLabel>
            <Select
              labelId="tipo"
              id="tipoTelefone"
              value={this.state.tipo ? this.state.tipo : 0}
              onChange={this.changeTipo}
              disabled={!AuthStorage.isAdmin()}
              variant="outlined"
              fullWidth>
              <MenuItem value={0}>RESIDENCIAL</MenuItem>
              <MenuItem value={1}>CELULAR</MenuItem>
              <MenuItem value={2}>COMERCIAL</MenuItem>
            </Select>
          </Grid>
          <Grid item xs={12}>
            <Button
              disabled={
                this.state.telefone == null ||
                this.state.tipo == null ||
                this.state.telefone.length === 0 ||
                this.state.telefone.includes("_") ||
                !AuthStorage.isAdmin()
              }
              variant="contained"
              color="primary"
              onClick={this.saveTelefone}
              fullWidth>
              Salvar Telefone
            </Button>
          </Grid>
        </Grid>
      </div>
    );
  }
}
