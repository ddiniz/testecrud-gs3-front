import "../App.css";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import ApiAuth from "../api/ApiAuth.js";
import LoginDTO from "../api/dto/LoginDTO.js";
import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";

class FormLogin extends React.Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
  };
  state = { login: new LoginDTO() };
  loginFunc = () => {
    var { history } = this.props;
    ApiAuth.login(this.state.login)
      .then(() => {
        history.push("/cliente");
      })
      .catch((e) => {
        console.log(e);
      });
  };
  setUsername = (e) => {
    this.setState((prevState) => ({
      login: {
        ...prevState.login,
        username: e.target.value,
      },
    }));
  };
  setPassword = (e) => {
    this.setState((prevState) => ({
      login: {
        ...prevState.login,
        password: e.target.value,
      },
    }));
  };
  render() {
    return (
      <div className="FormLogin">
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <TextField
              id="username"
              label="Username"
              variant="outlined"
              fullWidth
              value={this.state.login.username}
              onChange={this.setUsername}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              id="password"
              label="Password"
              variant="outlined"
              type="password"
              fullWidth
              value={this.state.login.password}
              onChange={this.setPassword}
            />
          </Grid>
          <Grid item xs={12}>
            <Button
              variant="contained"
              color="primary"
              onClick={this.loginFunc}
              fullWidth>
              Login
            </Button>
          </Grid>
        </Grid>
      </div>
    );
  }
}
export default withRouter(FormLogin);
