import "../App.css";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import React from "react";
import EnderecoDTO from "../api/dto/EnderecoDTO.js";
import ApiExterno from "../api/ApiExterno.js";
import InputMask from "react-input-mask";

import { checkString } from "../utils/FieldValidator.js";
import AuthStorage from "../api/AuthStorage.js";

export default class FormEndereco extends React.Component {
  state = {
    endereco: new EnderecoDTO(this.props.endereco),
    recebeu: false,
  };
  componentWillReceiveProps(nextProps) {
    if (
      !this.state.recebeu &&
      nextProps.endereco &&
      nextProps.endereco.id !== 0
    ) {
      this.setState({
        endereco: new EnderecoDTO(nextProps.endereco),
        recebeu: true,
      });
    }
  }

  changeCEP = (e) => {
    let val = e.target.value;
    this.setState((prevState) => ({
      endereco: {
        ...prevState.endereco,
        cep: val,
      },
    }));
    this.props.change(this.state.endereco);
    if (!val.includes("_")) {
      ApiExterno.getCEP(val)
        .then((response) => {
          this.setState((prevState) => ({
            endereco: {
              ...prevState.endereco,
              logradouro: response.data.logradouro,
              bairro: response.data.bairro,
              cidade: response.data.localidade,
              complemento: response.data.complemento,
              uf: this.ufToNumber(response.data.uf),
            },
          }));
        })
        .catch((e) => {
          console.log(e);
        })
        .finally(() => {
          this.props.change(this.state.endereco);
        });
    }
  };
  handleChangeLogradouro = (e) => {
    this.setState((prevState) => ({
      endereco: {
        ...prevState.endereco,
        logradouro: e.target.value,
      },
    }));
    this.props.change(this.state.endereco);
  };
  handleChangeBairro = (e) => {
    this.setState((prevState) => ({
      endereco: {
        ...prevState.endereco,
        bairro: e.target.value,
      },
    }));
    this.props.change(this.state.endereco);
  };
  handleChangeCidade = (e) => {
    this.setState((prevState) => ({
      endereco: {
        ...prevState.endereco,
        cidade: e.target.value,
      },
    }));
    this.props.change(this.state.endereco);
  };
  handleChangeComplemento = (e) => {
    this.setState((prevState) => ({
      endereco: {
        ...prevState.endereco,
        complemento: e.target.value,
      },
    }));
    this.props.change(this.state.endereco);
  };
  handleChangeNumero = (e) => {
    this.setState((prevState) => ({
      endereco: {
        ...prevState.endereco,
        numero: e.target.value,
      },
    }));
    this.props.change(this.state.endereco);
  };
  handleChangeUF = (e) => {
    this.setState((prevState) => ({
      endereco: {
        ...prevState.endereco,
        uf: e.target.value,
      },
    }));
    this.props.change(this.state.endereco);
  };
  render() {
    return (
      <div className="FormEndereco">
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <InputMask
              mask="99999-999"
              value={this.state.endereco.cep}
              disabled={!AuthStorage.isAdmin()}
              onChange={this.changeCEP}>
              {() => (
                <TextField
                  id="cep"
                  label="Cep"
                  variant="outlined"
                  fullWidth
                  helperText="Obrigatório"
                  error={!checkString(this.state.endereco.cep, 9)}
                />
              )}
            </InputMask>
          </Grid>
          <Grid item xs={12}>
            <TextField
              id="logradouro"
              label="Logradouro"
              variant="outlined"
              fullWidth
              value={this.state.endereco.logradouro}
              onChange={this.handleChangeLogradouro}
              helperText="Obrigatório"
              disabled={!AuthStorage.isAdmin()}
              error={!checkString(this.state.endereco.logradouro)}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              id="bairro"
              label="Bairro"
              variant="outlined"
              fullWidth
              value={this.state.endereco.bairro}
              onChange={this.handleChangeBairro}
              disabled={!AuthStorage.isAdmin()}
              helperText="Obrigatório"
              error={!checkString(this.state.endereco.bairro)}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              id="cidade"
              label="Cidade"
              variant="outlined"
              fullWidth
              value={this.state.endereco.cidade}
              onChange={this.handleChangeCidade}
              disabled={!AuthStorage.isAdmin()}
              helperText="Obrigatório"
              error={!checkString(this.state.endereco.cidade)}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              id="complemento"
              label="Complemento"
              variant="outlined"
              disabled={!AuthStorage.isAdmin()}
              fullWidth
              value={this.state.endereco.complemento}
              onChange={this.handleChangeComplemento}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              id="numero"
              label="Numero"
              variant="outlined"
              disabled={!AuthStorage.isAdmin()}
              value={this.state.endereco.numero}
              fullWidth
              onChange={this.handleChangeNumero}
            />
          </Grid>
          <Grid item xs={12}>
            <InputLabel id="ufSelect">UF</InputLabel>
            <Select
              labelId="ufSelect"
              id="uf"
              value={this.state.endereco.uf ? this.state.endereco.uf : 0}
              onChange={this.handleChangeUF}
              variant="outlined"
              disabled={!AuthStorage.isAdmin()}
              fullWidth>
              <MenuItem value={0}>AC</MenuItem>
              <MenuItem value={1}>AL</MenuItem>
              <MenuItem value={2}>AP</MenuItem>
              <MenuItem value={3}>AM</MenuItem>
              <MenuItem value={4}>BA</MenuItem>
              <MenuItem value={5}>CE</MenuItem>
              <MenuItem value={26}>DF</MenuItem>
              <MenuItem value={6}>ES</MenuItem>
              <MenuItem value={7}>GO</MenuItem>
              <MenuItem value={8}>MA</MenuItem>
              <MenuItem value={9}>MT</MenuItem>
              <MenuItem value={10}>MS</MenuItem>
              <MenuItem value={11}>MG</MenuItem>
              <MenuItem value={12}>PA</MenuItem>
              <MenuItem value={13}>PB</MenuItem>
              <MenuItem value={14}>PR</MenuItem>
              <MenuItem value={15}>PE</MenuItem>
              <MenuItem value={16}>PI</MenuItem>
              <MenuItem value={17}>RJ</MenuItem>
              <MenuItem value={18}>RN</MenuItem>
              <MenuItem value={19}>RS</MenuItem>
              <MenuItem value={20}>RO</MenuItem>
              <MenuItem value={21}>RR</MenuItem>
              <MenuItem value={22}>SC</MenuItem>
              <MenuItem value={23}>SP</MenuItem>
              <MenuItem value={24}>SE</MenuItem>
              <MenuItem value={25}>TO</MenuItem>
            </Select>
          </Grid>
        </Grid>
      </div>
    );
  }
  ufToNumber(uf) {
    switch (uf) {
      default:
      case "AC":
        return 0;
      case "AL":
        return 1;
      case "AP":
        return 2;
      case "AM":
        return 3;
      case "BA":
        return 4;
      case "CE":
        return 5;
      case "DF":
        return 26;
      case "ES":
        return 6;
      case "GO":
        return 7;
      case "MA":
        return 8;
      case "MT":
        return 9;
      case "MS":
        return 10;
      case "MG":
        return 11;
      case "PA":
        return 12;
      case "PB":
        return 13;
      case "PR":
        return 14;
      case "PE":
        return 15;
      case "PI":
        return 16;
      case "RJ":
        return 17;
      case "RN":
        return 18;
      case "RS":
        return 19;
      case "RO":
        return 20;
      case "RR":
        return 21;
      case "SC":
        return 22;
      case "SP":
        return 23;
      case "SE":
        return 24;
      case "TO":
        return 25;
    }
  }
}
