import "../App.css";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import React from "react";

export default class FormUsuario extends React.Component {
  salvar = () => {};
  render() {
    return (
      <div className="FormUsuario">
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <TextField
              id="username"
              label="Username"
              variant="outlined"
              fullWidth
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              id="password"
              label="Password"
              variant="outlined"
              fullWidth
            />
          </Grid>
          <Grid item xs={12}>
            <Button
              variant="contained"
              fullWidth
              color="primary"
              onClick={this.salvar}>
              Salvar
            </Button>
          </Grid>
        </Grid>
      </div>
    );
  }
}
