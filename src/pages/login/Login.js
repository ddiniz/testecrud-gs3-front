import "../../App.css";
import FormLogin from "../../components/FormLogin.js";
import React from "react";
import { withRouter } from "react-router-dom";
import AuthStorage from "../../api/AuthStorage";

class Login extends React.Component {
  componentDidMount() {
    var { history } = this.props;
    if (!AuthStorage.isLogado()) {
      history.replace("/");
    } else {
      history.replace("/cliente");
    }
  }
  render() {
    return (
      <div className="Login" style={{ marginTop: "10em" }}>
        <FormLogin />
      </div>
    );
  }
}
export default withRouter(Login);
