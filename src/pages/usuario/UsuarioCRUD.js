import "../../App.css";

import { DataGrid } from "@material-ui/data-grid";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import AuthStorage from "../../api/AuthStorage";
import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
class UsuarioCRUD extends React.Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
  };
  state = {
    headers: [
      { field: "id", headerName: "id", width: 70 },
      { field: "nome", headerName: "nome", width: 130 },
      { field: "permissao", headerName: "permissao", width: 130 },
    ],
    itens: [],
  };
  criar = () => {
    var { history } = this.props;
    history.push("/usuarioView");
  };
  componentDidMount() {
    if (!AuthStorage.isLogado()) {
      var { history } = this.props;
      history.replace("/");
    }
  }
  render() {
    return (
      <div className="UsuarioCRUD">
        Usuario CRUD
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <Button
              variant="contained"
              color="primary"
              onClick={this.criar}
              fullWidth>
              Criar novo usuário
            </Button>
          </Grid>
          <Grid item xs={12}>
            <DataGrid
              rows={this.state.itens}
              columns={this.state.headers}
              pageSize={5}
              checkboxSelection
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withRouter(UsuarioCRUD);
