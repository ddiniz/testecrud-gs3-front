import "../../App.css";
import AuthStorage from "../../api/AuthStorage";
import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import FormUsuario from "../../components/FormUsuario.js";
class UsuarioView extends React.Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
  };
  componentDidMount() {
    if (!AuthStorage.isLogado()) {
      var { history } = this.props;
      history.replace("/");
    }
  }
  render() {
    return (
      <div className="UsuarioView">
        Cliente View
        <FormUsuario />
      </div>
    );
  }
}

export default withRouter(UsuarioView);
