import "../../App.css";
import AuthStorage from "../../api/AuthStorage";
import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";

import FormCliente from "../../components/FormCliente.js";
class ClienteView extends React.Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
  };
  componentDidMount() {
    if (!AuthStorage.isLogado()) {
      var { history } = this.props;
      history.replace("/");
    }
  }
  render() {
    return (
      <div className="ClienteView">
        <FormCliente
          id={
            this.props.location && this.props.location.state
              ? this.props.location.state.id
              : null
          }
        />
      </div>
    );
  }
}

export default withRouter(ClienteView);
