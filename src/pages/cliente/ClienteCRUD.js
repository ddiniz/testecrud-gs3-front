import "../../App.css";

import { DataGrid } from "@material-ui/data-grid";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import AuthStorage from "../../api/AuthStorage";
import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import ApiCliente from "../../api/ApiCliente.js";

import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import VisibilityIcon from "@material-ui/icons/Visibility";
import ApiAuth from "../../api/ApiAuth.js";

class ClienteCRUD extends React.Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
  };
  state = {
    headers: [
      { field: "id", headerName: "id", width: 70 },
      { field: "nome", headerName: "nome", width: 250 },
      { field: "cpf", headerName: "CPF", width: 200 },
      {
        field: "",
        headerName: "Ação",
        disableClickEventBubbling: true,
        width: 300,
        renderCell: (params) => {
          const deletar = () => {
            if (window.confirm(`Deletar cliente ${params.getValue("nome")}?`)) {
              ApiCliente.delete(params.getValue("id"))
                .then((response) => {
                  this.updateLista();
                  alert("Deletado com sucesso.");
                })
                .catch((e) => {
                  console.log(e);
                  alert(`Erro ao deletar:${e}`);
                });
            }
          };
          const editar = () => {
            var { history } = this.props;
            history.push("/clienteView", { id: params.getValue("id") });
          };

          const visualizar = () => {
            var { history } = this.props;
            history.push("/clienteView", { id: params.getValue("id") });
          };

          return (
            <div>
              {AuthStorage.isAdmin() ? (
                <div>
                  <IconButton
                    aria-label="edit"
                    onClick={editar}
                    disabled={!AuthStorage.isAdmin()}>
                    <EditIcon />
                  </IconButton>
                  <IconButton
                    aria-label="delete"
                    onClick={deletar}
                    disabled={!AuthStorage.isAdmin()}>
                    <DeleteIcon />
                  </IconButton>
                </div>
              ) : (
                <div>
                  <IconButton aria-label="edit" onClick={visualizar}>
                    <VisibilityIcon />
                  </IconButton>
                </div>
              )}
            </div>
          );
        },
      },
    ],
    itens: [],
  };
  criar = () => {
    var { history } = this.props;
    history.push("/clienteView");
  };
  updateLista() {
    ApiCliente.list().then((response) => {
      this.setState({ itens: response.data });
    });
  }
  componentDidMount() {
    if (!AuthStorage.isLogado()) {
      var { history } = this.props;
      history.replace("/");
    }
    this.updateLista();
  }
  logoff = () => {
    ApiAuth.logoff();
    var { history } = this.props;
    history.replace("/");
  };
  render() {
    return (
      <div className="ClienteCRUD" style={{ marginTop: "5em" }}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <Button
              variant="contained"
              color="primary"
              onClick={this.criar}
              fullWidth>
              Criar novo cliente
            </Button>
          </Grid>
          <Grid item xs={12} style={{ height: "50em" }}>
            <DataGrid
              rows={this.state.itens}
              columns={this.state.headers}
              pageSize={10}
            />
          </Grid>
          <Button
            variant="contained"
            color="primary"
            onClick={this.logoff}
            fullWidth>
            Logoff
          </Button>
        </Grid>
      </div>
    );
  }
}

export default withRouter(ClienteCRUD);
