import "./App.css";
import Login from "./pages/login/Login.js";
import ClienteCRUD from "./pages/cliente/ClienteCRUD.js";
import ClienteView from "./pages/cliente/ClienteView.js";
import UsuarioCRUD from "./pages/usuario/UsuarioCRUD.js";
import UsuarioView from "./pages/usuario/UsuarioView.js";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Container from "@material-ui/core/Container";
function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Container maxWidth="md">
            <Route exact path="/" component={Login} />
            <Route path="/cliente" component={ClienteCRUD} />
            <Route path="/clienteView" component={ClienteView} />
            <Route path="/usuario" component={UsuarioCRUD} />
            <Route path="/usuarioView" component={UsuarioView} />
          </Container>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
