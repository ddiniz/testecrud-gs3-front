export function checkString(val, len = -1) {
  if (val == null) return false;
  return val.length !== 0 && (len === -1 ? true : val.length === len);
}
export function checkStringMinMax(val, min = 0, max = -1) {
  if (val == null) return false;
  return val.length >= min && (max === -1 ? true : val.length <= max);
}
export function checkNumber(val, min = -1, max = -1) {
  if (val == null) return false;
  return (min === -1 ? true : val <= min) && (max === -1 ? true : val >= max);
}
export function checkObject(obj) {
  return obj != null;
}
export function checkEmail(email) {
  let regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regex.test(email);
}
export function checkCPF(cpf) {
  if (cpf == null) return false;
  cpf = cpf.split(".").join("");
  cpf = cpf.split("-").join("");
  if (cpf.length !== 11) return false;
  let aux = [
    "00000000000",
    "11111111111",
    "22222222222",
    "33333333333",
    "44444444444",
    "55555555555",
    "66666666666",
    "77777777777",
    "88888888888",
    "99999999999",
  ];
  for (let i = 0; i < 10; i++) {
    if (cpf === aux[i]) return false;
  }

  let soma = 0;
  let peso = 10;
  for (let i = 0; i < 9; i++, peso--) {
    soma += cpf[i] * peso;
  }
  let resto = soma % 11;
  let digito1 = soma % 11 < 2 ? 0 : 11 - resto;

  soma = 0;
  peso = 11;
  for (let i = 0; i < 10; i++, peso--) {
    soma += cpf[i] * peso;
  }
  resto = soma % 11;
  let digito2 = soma % 11 < 2 ? 0 : 11 - resto;

  if (digito1 === parseInt(cpf[9]) && digito2 === parseInt(cpf[10]))
    return true;
  else return false;
}

export function validateEmail(obj) {
  return checkString(obj.email);
}
export function validateCliente(cliente) {
  return checkString(cliente.nome) && checkCPF(cliente.cpf);
}
export function validateEndereco(endereco) {
  return (
    checkString(endereco.cep, 9) &&
    checkString(endereco.logradouro) &&
    checkString(endereco.bairro) &&
    checkString(endereco.cidade) &&
    checkString(endereco.uf)
  );
}
export function validateLogin(login) {
  return checkString(login.username) && checkString(login.password);
}
export function validateTelefone(telObj) {
  return checkString(telObj.telefone, 12) && checkNumber(telObj.tipo);
}
export function validateUsuario(usuario) {
  return (
    checkString(usuario.username) &&
    checkString(usuario.password) &&
    checkNumber(usuario.permissao)
  );
}
